
def spanish_and_brazilian_fruits(spanish_fruits, brazilian_fruits):
    output = brazilian_fruits & spanish_fruits   
    return list(output)

def spanish_and_japan_fruits(spanish_fruits, japanese_fruits):
    output = spanish_fruits & japanese_fruits
    return list(output)

def brazilian_and_japan_fruits(brazilian_fruits, japanese_fruits):
    output = brazilian_fruits & japanese_fruits
    return list(output)

def popular_spanish_or_brazilian_fruits(popular_fruits, spanish_fruits, brazilian_fruits):
    output = popular_fruits & (brazilian_fruits | spanish_fruits)
    return list(output)
    
def popular_only_spanish_fruits(popular_fruits, spanish_fruits, japanese_fruits, brazilian_fruits):
    output = popular_fruits & (spanish_fruits - (japanese_fruits | brazilian_fruits))
    return list(output)

def only_yahoo_emails(emails_list):
    conjunto = {x for x in emails_list if 'yahoo' in x}
    return conjunto

def only_hotmail_emails(emails_list):
    conjunto = {x for x in emails_list if 'hotmail' in x}
    return conjunto

def only_br_emails(emails_list):
    conjunto = {x for x in emails_list if x.endswith('br')}
    return conjunto
